import * as types from '../actions/actionTypes';
import initialState from './initialState';

var twitterReducer = (state = initialState.twitter, action) => {
    switch(action.type) {
    	case types.SEARCH_TWEETS_IS_LOADING:
            return {...state, isLoadingSearch: action.isLoading}
        case types.SEARCH_TWEETS_SUCCESS:
            return {...state, searchResults: action.response.statuses}
        case types.GET_TWITTER_LIKED_IS_LOADING:
            return {...state, isLoadingLikedTweets: action.isLoading}
        case types.GET_TWITTER_LIKED_SUCCESS:
            return {...state, twitterLikedTweets: action.response.statuses}
        case types.GET_TWITTER_TIMELINE_IS_LOADING:
            return {...state, isLoadingTimeline: action.isLoading}
        case types.GET_TWITTER_TIMELINE_SUCCESS:
            return {...state, twitterTimelineResults: action.response}
        case types.GET_TWITTER_FRIENDS_IS_LOADING:
            return {...state, isLoadingFriends: action.isLoading}
        case types.GET_TWITTER_FRIENDS_SUCCESS:
            return {...state, twitterFriendsResults: action.response.users}
        case types.GET_TWITTER_USER_TWEETS_IS_LOADING:
            return {...state, isLoadingUserResult: action.isLoading}
        case types.GET_TWITTER_USER_TWEETS_SUCCESS:
            return {...state, twitterUserResult: action.response}
        case types.POST_TWITTER_TWEET_SUCCESS:
            return{...state, lastPostedTweet: action.response}
        default:
            return state;
    }
};

export default twitterReducer;