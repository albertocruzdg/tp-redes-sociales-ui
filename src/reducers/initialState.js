export default {
    twitter: {
        isLoadingSearch: false, 
        searchResults: [], 
        isLoadingTimeline: false, 
        twitterTimelineResults: [],
        isLoadingFriends: false, 
        twitterFriendsResults: [],
        isLoadingUserResult: false, 
        twitterUserResult: [],
        isLoadingLikedTweets: false, 
        twitterLikedTweets: []
    },
    header: {
        selectedMenu: '/',
    }
};