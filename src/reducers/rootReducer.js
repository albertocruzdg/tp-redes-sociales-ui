import { combineReducers } from 'redux';
import twitterReducer from './twitterReducer';
import headerReducer from './headerReducer';

const rootReducer = combineReducers({
    twitterReducer,
    headerReducer,
});

export default rootReducer;