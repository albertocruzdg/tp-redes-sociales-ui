import * as types from '../actions/actionTypes';
import initialState from './initialState';

var headerReducer = (state = initialState.header, action) => {
    switch(action.type) {
    	case types.SELECTED_MENU_CHANGED:
            return {...state, selectedMenu: action.selectedMenu}
        default:
            return state;
    }
};

export default headerReducer;