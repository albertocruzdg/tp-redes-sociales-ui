import React, { Component } from 'react';

class HomePage extends Component {
    render() {
        return (
            <div>
                <h1 className="jumbotron text-center">Integración de Aplicaciones con Redes Sociales</h1>
                <p>
                    Alumnos:
                    <ul>
                        <li>Alberto Cruz</li>
                        <li>Emanuel Kotzayan</li>
                    </ul>
                </p>
            </div>
        );
    }
}

export default HomePage;