import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getTimeline } from '../../actions/twitterActions';
import TweetList from './TweetList';

class TwitterTimelinePage extends Component {
    componentDidMount() {
        this.props.getTimeline();
    }

    render() {
        return(
            <div>
                <TweetList tweets={this.props.twitterTimelineResults}
                    isLoading={this.props.twitterIsLoadingTimeline}/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    twitterTimelineResults: state.twitterReducer.twitterTimelineResults,
    twitterIsLoadingTimeline: state.twitterReducer.isLoadingTimeline
});

const mapDispatchToProps = dispatch => ({
    getTimeline: () => dispatch(getTimeline()),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
    withRef: true
})(TwitterTimelinePage);