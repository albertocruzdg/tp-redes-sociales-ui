import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getFriends } from '../../actions/twitterActions';
import TwitterFriendList from './TwitterFriendList';

class TwitterFriendsPage extends Component {
    componentDidMount() {
        this.props.getFriends();
    }

    render() {
        return(
            <div>
                <TwitterFriendList friends={this.props.twitterFriendsResults} 
                    isLoading={this.props.twitterIsLoadingFriends}/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    twitterFriendsResults: state.twitterReducer.twitterFriendsResults,
    twitterIsLoadingFriends: state.twitterReducer.isLoadingFriends
});

const mapDispatchToProps = dispatch => ({
    getFriends: () => dispatch(getFriends()),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
    withRef: true
})(TwitterFriendsPage);