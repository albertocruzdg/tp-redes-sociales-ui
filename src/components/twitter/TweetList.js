import React, { Component } from 'react';
import Loader from 'react-loader-spinner';

import Tweet from './Tweet';

class TweetList extends Component {
	getTweetList() {
		if (this.props.tweets && this.props.tweets.length > 0 ) {
			return (this.props.tweets.map((tweet, index) => (
		            <Tweet
		              key={tweet.id_str}
		              text={tweet.text}
		              user={tweet.user}
		              date={tweet.created_at}
					  className="m-4"
		            />)))
		} else {
			return (<div className="alert alert-warning col">No hay tweets para mostrar.</div>)
		}
	}

    render() {
        return(
            <div className="m-t-2">
            	{this.props.isLoading ? (<div style={{marginLeft: 'calc(50% - 50px)'}}><Loader 
			         type="TailSpin"
			         color="#00BFFF"
			         height="100"	
			         width="100"
			     /></div>) : this.getTweetList()}
            </div>
        );
    }
}

export default TweetList;