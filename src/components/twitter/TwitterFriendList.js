import React, { Component } from 'react';
import Loader from 'react-loader-spinner';

import TwitterFriend from './TwitterFriend';

class TwitterFriendList extends Component {
	getFriendList() {
		if (this.props.friends && this.props.friends.length > 0) {
			return (this.props.friends.map((friend, index) => (
		            <TwitterFriend
		              key={friend.id_str}
		              name={friend.name}
		              screenName={friend.screen_name}
					  imageUrl={friend.profile_image_url}
					  description={friend.description}
					  className="col-4 m-3"
		            />
		          )))
		} else {
			return (<div className="alert alert-warning col"> No hay usuarios para mostrar.</div>)
		}
	}

    render() {
        return(
            <div className="row">
            	{this.props.isLoading ? (<div style={{marginLeft: 'calc(50% - 50px)'}}><Loader 
			         type="TailSpin"
			         color="#00BFFF"
			         height="100"	
			         width="100"
			     /></div>) : this.getFriendList()}
            </div>
        );
    }
}

export default TwitterFriendList;