import React, { Component } from 'react';
import { connect } from "react-redux";

import TwitterUserSearchForm from './TwitterUserSearchForm';
import TweetList from './TweetList';

class TwitterTweetsByUserPage extends Component
{
    render() {
        return (
            <div className="border p-4">
                <TwitterUserSearchForm/>
                <TweetList tweets={this.props.twitterUserResult} 
                    isLoading={this.props.twitterIsLoadingUserResult} />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    twitterUserResult: state.twitterReducer.twitterUserResult,
    twitterIsLoadingUserResult: state.twitterReducer.isLoadingUserResult
});

export default connect(mapStateToProps, null, null, {
    withRef: true
})(TwitterTweetsByUserPage);