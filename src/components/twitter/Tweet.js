import React, { Component } from 'react';

import moment from 'moment';
import 'moment/locale/es';

import UserTag from './UserTag';

class Tweet extends Component {
	timeDifference(date) {
		return moment(date, 'dd MMM DD HH:mm:ss ZZ YYYY', 'en').locale("ES").fromNow();
	}

    render() {
    	return (
    		<div className={this.props.className + " card"}>
				<div className="card-body row">
					<figure className="col-1">
						<img className="mw-10 rounded-circle" alt="" src={this.props.user.profile_image_url}/>
					</figure>
					
					<div className="col-11">
						<div><UserTag user={this.props.user} leadingText='dijo:'/></div>
						<div>{this.props.text}</div>
						<i>{this.timeDifference(this.props.date)}</i>
					</div>
					
				</div>
    			
        	</div>
        )
    }
}

export default Tweet