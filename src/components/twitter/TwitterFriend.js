import React, { Component } from 'react';

class TwitterFriend extends Component
{
    render() {
        return(
            <div className={this.props.className + " card p-5"}>
                <img className="mw-10 card-img-top rounded-circle" src={this.props.imageUrl} alt=""/>
                <div className="card-body pt-4">
                    <p className="card-title">@{this.props.screenName}, {this.props.name}</p>
                    <i> {this.props.description} </i>
                </div>
            </div>
        );
    }
}

export default TwitterFriend;