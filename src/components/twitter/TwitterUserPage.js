import React, { Component } from 'react';
import { connect } from 'react-redux';

import TweetList from './TweetList';
import { getUserTweets } from '../../actions/twitterActions'

class TwitterUserPage extends Component {
    constructor(props) {
        super(props);
        
        this.state = { userName: null };
    }

    componentDidMount() {
        const userName = window.location.href.split('/')[5];

        this.setState({
            userName: userName
        });

        this.props.getUserTweets(userName);
    }

    render() {
        return(
            <div>
                { this.state.userName ? (<h1> Tweets de {this.state.userName} </h1>) : ''}
                <TweetList tweets={this.props.twitterUserResult} 
                    isLoading={this.props.twitterIsLoadingUserResult} />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    twitterUserResult: state.twitterReducer.twitterUserResult,
    twitterIsLoadingUserResult: state.twitterReducer.isLoadingUserResult
});

const mapDispatchToProps = dispatch => ({
    getUserTweets: text => dispatch(getUserTweets(text)),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
  withRef: true
})(TwitterUserPage);