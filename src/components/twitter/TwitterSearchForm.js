import React, { Component } from 'react';
import { connect } from "react-redux";

import { searchTweets } from '../../actions/twitterActions'

class TwitterSearchForm extends Component {
    constructor(props) {
        super(props);
        
        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        this.props.searchTweets(this.state.value);
        event.preventDefault();
    }

    render() {
        return(
            <form className="mb-4" onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <input className="form-control" type="text" placeholder="Palabra a buscar" value={this.state.value} onChange={this.handleChange} />
                </div>
                <input className="btn btn-primary btn-lg btn-block" type="submit" value="Search" />
            </form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
  searchTweets: text => dispatch(searchTweets(text)),
});

export default connect(null, mapDispatchToProps, null, {
  withRef: true
})(TwitterSearchForm);