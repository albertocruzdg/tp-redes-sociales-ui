import React, { Component } from 'react';
import { connect } from "react-redux";

import TwitterNewTweetForm from './TwitterNewTweetForm';
import Tweet from './Tweet';

class TwitterNewTweetPage extends Component {
    render() {
        return(
            <div className="border p-4">
                <TwitterNewTweetForm />
                {this.props.tweet ? (
                    <Tweet user={this.props.tweet.user} 
                        text={this.props.tweet.text} 
                        date={this.props.tweet.created_at}/>
                ) : (
                    <p> </p>
                )}
            </div>
        );
    }
}

const mapStateToProps = state => ({
  tweet: state.twitterReducer.lastPostedTweet,
});

export default connect(mapStateToProps, null, null, {
  withRef: true
})(TwitterNewTweetPage);
