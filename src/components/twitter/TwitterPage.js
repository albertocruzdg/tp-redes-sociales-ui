import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import NavItem from '../common/NavItem';

const TwitterPage = withRouter(props => <TwitterPageInternal {...props}/>);

class TwitterPageInternal extends Component {
    render() {
        return(
            <div>
                <h1 className="jumbotron text-center">Twitter</h1>
                <ul className="nav nav-pills nav-fill mt-2 mb-4">
                    <NavItem to="/twitter/timeline" label="Timeline" currentPath={this.props.location.pathname}/>
                    <NavItem to="/twitter/search" label="Buscar" currentPath={this.props.location.pathname} />
                    <NavItem to="/twitter/search/user" label="Buscar tweets por usuario" currentPath={this.props.location.pathname}/>
                    <NavItem to="/twitter/friends" label="Amigos" currentPath={this.props.location.pathname}/>
                    <NavItem to="/twitter/post" label="Nuevo tweet" currentPath={this.props.location.pathname}/>
                </ul>
            </div>
        );
    }
}

export default TwitterPage;