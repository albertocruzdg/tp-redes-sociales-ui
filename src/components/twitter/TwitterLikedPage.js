import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getLiked } from '../../actions/twitterActions';
import TweetList from './TweetList';

class TwitterLikedPage extends Component {
    componentDidMount() {
        this.props.getLiked();
    }

    render() {
        return(
            <div>
                <TweetList tweets={this.props.twitterLikedTweets}
                    isLoading={this.props.twitterIsLoadingLikedTweets}/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    twitterLikedTweets: state.twitterReducer.twitterLikedTweets,
    twitterIsLoadingLikedTweets: state.twitterReducer.isLoadingLikedTweets
});

const mapDispatchToProps = dispatch => ({
    getLiked: () => dispatch(getLiked()),
});

export default connect(mapStateToProps, mapDispatchToProps, null, {
    withRef: true
})(TwitterLikedPage);