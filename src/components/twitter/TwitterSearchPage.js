import React, { Component } from 'react';
import { connect } from "react-redux";

import TwitterSearchForm from './TwitterSearchForm';
import TweetList from './TweetList';

class TwitterSearchPage extends Component {
    render() {
        return(
            <div className="border p-4">
                <TwitterSearchForm />
                <TweetList tweets={this.props.twitterSearchResult} 
                	isLoading={this.props.twitterIsLoadingSearch}/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
  twitterSearchResult: state.twitterReducer.searchResults,
  twitterIsLoadingSearch: state.twitterReducer.isLoadingSearch
});

export default connect(mapStateToProps, null, null, {
  withRef: true
})(TwitterSearchPage);
