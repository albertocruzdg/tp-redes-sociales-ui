import React, { Component } from 'react';
import { connect } from "react-redux";
import { Line } from 'rc-progress';

import { postTweet } from '../../actions/twitterActions'

class TwitterNewTweetForm extends Component {
    TWITTER_MAX_LENGTH = 280;

    constructor(props) {
        super(props);

        this.state = { value: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        this.props.postTweet(this.state.value);
        event.preventDefault();
    }

    progressPercentage() {
        return (this.state.value.length / this.TWITTER_MAX_LENGTH)*100;
    }

    progressColor() {
        const currentPercentage = this.progressPercentage();
        if (currentPercentage > 85) {
            return "#F44336"
        } else if (currentPercentage > 50) {
            return "#FF9800"
        } else {
            return "#4CAF50"
        }
    }

    render() {
        return(
            <form className="mb-4" onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <textarea className="col" placeholder="Tu nuevo tweet" maxLength={this.TWITTER_MAX_LENGTH} value={this.state.value} onChange={this.handleChange}></textarea>
                    <Line percent={this.progressPercentage()} strokeWidth="1" strokeColor={this.progressColor()} />
                    <div style={{textAlign: 'right'}}> {this.state.value.length}/{this.TWITTER_MAX_LENGTH} </div>
                </div>
                <input className="btn btn-primary btn-lg btn-block" type="submit" value="Enviar Tweet" />
            </form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    postTweet: text => dispatch(postTweet(text)),
});

export default connect(null, mapDispatchToProps, null, {
    withRef: true
})(TwitterNewTweetForm);