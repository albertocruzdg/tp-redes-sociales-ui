import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class UserTag extends Component {
	getUserLink() {
		if (this.props.user) {
			return (<div> <Link to={"/twitter/user/" + this.props.user.screen_name}>{this.props.user.name} (@{this.props.user.screen_name})</Link> 
						{this.props.leadingText ? ' ' + this.props.leadingText : ''}</div>)
		} else {
			return ''
		}
	}

    render() {
    	return (<div>
    		{this.getUserLink()}
    		</div>)
    }
}

export default UserTag