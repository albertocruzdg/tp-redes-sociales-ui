import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import NavItem from './NavItem';

const Header = withRouter(props => <HeaderInternal {...props}/>);

class HeaderInternal extends Component {
    render() {
        return (
            <header>
                <nav className="mt-2 mb-2">
                    <ul className="nav nav-pills">
                        <NavItem to="/" label="Home" currentPath={this.props.location.pathname}/>
                        <TwitterNavItem to="/twitter" label="Twitter" currentPath={this.props.location.pathname}/>
                    </ul>
                </nav>
                
            </header>
        );
    }
};

class TwitterNavItem extends NavItem
{
    isActive() {
        return this.props.currentPath.startsWith(this.props.to);
    }
}

export default Header;