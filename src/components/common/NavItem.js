import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NavItem extends Component {
    isActive() {
        return this.props.currentPath === this.props.to;
    }

    getActiveClass() {
        if (this.isActive()) {
            return ' active';
        }

        return ''
    }

    render() {
        return(
            <li className="nav-item">
                <Link className={"nav-link" + this.getActiveClass()} to={this.props.to}>{this.props.label}</Link>
            </li>
        );
    }
}

export default NavItem;