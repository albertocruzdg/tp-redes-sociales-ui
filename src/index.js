import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { BrowserRouter, Route} from 'react-router-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import configureStore from './stores/configureStore';
import HomePage from './components/HomePage';
import TwitterPage from './components/twitter/TwitterPage';
import TwitterSearchPage from './components/twitter/TwitterSearchPage';
import TwitterTimelinePage from './components/twitter/TwitterTimelinePage';
import TwitterFriendsPage from './components/twitter/TwitterFriendsPage';
import TwitterTweetsByUserPage from './components/twitter/TwitterTweetsByUserPage';
import TwitterNewTweetPage from './components/twitter/TwitterNewTweetPage';
import TwitterUserPage from './components/twitter/TwitterUserPage';
import TwitterLikedPage from './components/twitter/TwitterLikedPage';
import Header from './components/common/Header'

import 'bootstrap/dist/css/bootstrap.css';

const store = configureStore();

//store.dispatch(loadConfigs());

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter >
            <div>
                <Header></Header>
                <Route exact path="/" component={HomePage} />
                <Route path="/twitter" component={TwitterPage} />
                <Route exact path="/twitter/search" component={TwitterSearchPage} />
                <Route path="/twitter/search/user" component={TwitterTweetsByUserPage} />
                <Route path="/twitter/timeline" component={TwitterTimelinePage} />
                <Route path="/twitter/liked" component={TwitterLikedPage} />
                <Route path="/twitter/friends" component={TwitterFriendsPage} />
                <Route path="/twitter/post" component={TwitterNewTweetPage} />
                <Route path="/twitter/user" component={TwitterUserPage} />
            </div>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
