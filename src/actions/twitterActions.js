import twitterApi from '../api/TwitterApi';
import * as types from './actionTypes';

export const searchTweets = (text) => dispatch => {
    dispatch(searchTweetsIsLoading(true));

    return twitterApi
        .searchTweets(text)
        .then(response => {
            dispatch(searchTweetsIsLoading(false));
            dispatch(searchTweetsSuccess(response));
        }).catch(error => {
            dispatch(searchTweetsIsLoading(false));
            throw error;
        });
}

export const searchTweetsIsLoading = (isLoading) => {
    return {
        type: types.SEARCH_TWEETS_IS_LOADING,
        isLoading
    };
};

export const searchTweetsSuccess = (response) => {
    return {
        type: types.SEARCH_TWEETS_SUCCESS,
        response
    };
};

export const getTimeline = () => dispatch => {
    dispatch(getTimelineIsLoading(true));

    return twitterApi
        .getTimeLine()
        .then(response => {
            dispatch(getTimelineIsLoading(false));
            dispatch(getTimelineSuccess(response));
        }).catch(error =>{
            dispatch(getTimelineIsLoading(false));
            throw error;
        });

}

export const getTimelineIsLoading = (isLoading) => {
    return {
        type: types.GET_TWITTER_TIMELINE_IS_LOADING,
        isLoading
    };
};

export const getTimelineSuccess = (response) => {
    return {
        type: types.GET_TWITTER_TIMELINE_SUCCESS,
        response
    };
};

export const getLiked = () => dispatch => {
    dispatch(getLikedIsLoading(true));

    return twitterApi
        .getLiked()
        .then(response => {
            dispatch(getLikedIsLoading(false));
            dispatch(getTimelineSuccess(response));
        }).catch(error =>{
            dispatch(getLikedSuccess(false));
            throw error;
        });

}

export const getLikedIsLoading = (isLoading) => {
    return {
        type: types.GET_TWITTER_TIMELINE_IS_LOADING,
        isLoading
    };
};

export const getLikedSuccess = (response) => {
    return {
        type: types.GET_TWITTER_TIMELINE_SUCCESS,
        response
    };
};

export const getFriends = () => dispatch => {
    dispatch(getFriendsIsLoading(true));

    return twitterApi
        .getFriends()
        .then(response => {
            dispatch(getFriendsIsLoading(false));
            dispatch(getFriendsSuccess(response));
        }).catch(error =>{
            dispatch(getFriendsIsLoading(false));
            throw error;
        });

}

export const getFriendsIsLoading = (isLoading) => {
    return {
        type: types.GET_TWITTER_FRIENDS_IS_LOADING,
        isLoading
    };
};

export const getFriendsSuccess = (response) => {
    return {
        type: types.GET_TWITTER_FRIENDS_SUCCESS,
        response
    };
};

export const getUserTweets = (username) => dispatch => {
    dispatch(getUserTweetsIsLoading(true));

    return twitterApi
        .getUserTweets(username)
        .then(response => {
            dispatch(getUserTweetsIsLoading(false));
            dispatch(getUserTweetsSuccess(response));
        }).catch(error =>{
            dispatch(getUserTweetsIsLoading(false));
            throw error;
        });

}

export const getUserTweetsIsLoading = (isLoading) => {
    return {
        type: types.GET_TWITTER_USER_TWEETS_IS_LOADING,
        isLoading
    };
};

export const getUserTweetsSuccess = (response) => {
    return {
        type: types.GET_TWITTER_USER_TWEETS_SUCCESS,
        response
    };
};

export const postTweet = (text) => dispatch => {
    return twitterApi
        .postTweet(text)
        .then(response => {
            dispatch(postTweetSuccess(response));
        }).catch(error =>{
            throw error;
        });

}

export const postTweetSuccess = (response) => {
    return {
        type: types.POST_TWITTER_TWEET_SUCCESS,
        response
    };
};