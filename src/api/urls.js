export const BASE_URL = 'http://159.89.152.129:5000/'

export const Twitter = {
    SEARCH: BASE_URL + 'twitter/search/',
    TIMELINE: BASE_URL + 'twitter/timeline',
    LIKED: BASE_URL + 'twitter/liked',
    FRIENDS: BASE_URL + 'twitter/friends',
    TWEETS: BASE_URL + 'twitter/tweets/',
    TWEET: BASE_URL + 'twitter/tweet',
};