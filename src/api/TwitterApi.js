import * as urls from './urls';

class TwitterApi {
    static searchTweets(text) {
        return fetch(urls.Twitter.SEARCH + text)
	        .then(response => {
	            return response.json();
	        })
	        .catch(error=>{
	            return error;
	        });
	}
	
	static getTimeLine() {
		return fetch(urls.Twitter.TIMELINE)
			.then(response => {
				return response.json();
			}).catch(error => {
				return error;
			});
	}

	static getLiked() {
		return fetch(urls.Twitter.LIKED)
			.then(response => {
				return response.json();
			}).catch(error => {
				return error;
			});
	}

	static getFriends() {
		return fetch(urls.Twitter.FRIENDS)
		.then(response => {
			return response.json();
		}).catch(error => {
			return error;
		});
	}

	static getUserTweets(username) {
		return fetch(urls.Twitter.TWEETS + username)
		.then(response => {
			return response.json();
		}).catch(error => {
			return error;
		});
	}

	static async postTweet(text) {
		let result = await fetch(urls.Twitter.TWEET, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				tweet: text
			})
		})
		
		return await result.json();
	}
}

export default TwitterApi;