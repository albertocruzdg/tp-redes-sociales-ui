import { applyMiddleware, createStore } from 'redux';
import rootReducer from '../reducers/rootReducer';
import thunk from 'redux-thunk';

var configureStore = () => createStore(rootReducer, applyMiddleware(thunk));

export default configureStore;